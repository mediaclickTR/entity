<?php

return [
    'entities'=>'Kişiler/Kurumlar',
    'create-entity'=>'Kişi/Kurum oluştur',
    'edit-entity'=>'Kişi/Kurum Düzenle',
    'name-entity'=>'Kişi/Kurum Adı',
    'surname-entity'=>'Kişi/Kurum Soyadı',
    'email-entity'=>'Kişi/Kurum E-Posta',
    'description-entity'=>'Kişi/Kurum Açıklaması',
];