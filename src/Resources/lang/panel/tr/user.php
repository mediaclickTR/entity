<?php

return [
    'users'=>'Üyeler',
    'website'=>'Web Site',
    'create-user'=>'Üye Ekle',
    'edit-user'=>'Üye Düzenle',
    'name-user'=>'Üye Adı Soyadı',
    'email-user'=>'Üye E-Posta',
    'password-user'=>'Üye Şifre',
    'repeat-password-user'=>'Üye Şifre Tekrar',
];