<?php

return [
    'userlists'=>'Üye Listesi',
    'list-members'=>'Listeye Ait Üyeler',
    'list-members-create'=>'Listeye Üye Ekle',
    'edit-userlist'=>'Listeyi Güncelle',
    'name-userlist'=>'Liste Adı',
    'description-userlist'=>'Liste Açıklaması',
    'create-userlist'=>'Liste Oluştur',
];