@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <div class="title">{!! trans("EntityPanel::entity.edit-entity") !!}</div>
        <form action="{!! route('Entity.entities.update') !!}" novalidate method="POST">
            @include("EntityPanel::entities.form")
        </form>
    </div>
@endsection