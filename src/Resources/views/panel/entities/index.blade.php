@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <div class="title">{!! trans("EntityPanel::entity.entities") !!}</div>
            <div class="float-right">
                <a class="btn btn-primary" href="{!! route('Entity.entities.create') !!}"> <i class="fa fa-plus"></i> {!! trans("MPCorePanel::general.new_add") !!}</a>
            </div>
             <div class="clearfix"></div>
            <table>
                    <thead>
                    <tr>
                        <th>{!! trans("MPCorePanel::general.name") !!}</th>
                        <th>{!! trans("MPCorePanel::general.email") !!}</th>
                        <th>{!! trans("MPCorePanel::general.website") !!}</th>
                        <th>{!! trans("MPCorePanel::general.created_at") !!}</th>
                        <th>{!! trans("MPCorePanel::general.actions") !!}</th>
                    </tr>
                    </thead>
                     @foreach($entities as $entity)
                    <tr>
                    <td>{!! $entity->name !!}</td>
                    <td>{!! $entity->email !!}</td>
                    <td>
                        @if(isset($entity->model->first()->entitylist->website))
                            @foreach($entity->model as $row)
                                {!! (isset($row->entitylist->website->first()->domain)) ? $row->entitylist->website->first()->domain : '-'  !!}
                            @endforeach
                        @endif
                    </td>
                    <td>{!! $entity->created_at !!}</td>
                    <td>
                        <select class="nice" onchange="locationOnChange(this.value);">
                            <option value="#">{!! trans("MPCorePanel::general.selection") !!}</option>
                            <option value="{!! route('Entity.entities.edit',$entity->id) !!}">{!! trans("MPCorePanel::general.edit") !!}</option>
                            <option value="{!! route('Entity.entities.delete',$entity->id ) !!}">{!! trans("MPCorePanel::general.delete") !!}</option>
                        </select>
                    </td>
                </tr>
                @endforeach
            </table>
            <div class="bottom-detail">
                <div class="float-left">
                    {!! $entities->links() !!}
                </div>
                <div class="float-right">
                    <select class="nice">
                        <option>10</option>
                        <option>15</option>
                        <option>20</option>
                        <option>25</option>
                    </select>
                </div>
            </div>
        </div>
@endsection
