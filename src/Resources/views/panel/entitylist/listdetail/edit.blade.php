@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <div class="title">{!! trans("EntityPanel::entitylistdetail.user-update") !!}</div>
        <form action="{!! route('Entity.entitylist.listdetail.update',$list_id) !!}" novalidate method="POST">
            @csrf
            @if(isset($user))
                <input type="hidden" name="id" value="{!! $user->id !!}">
            @endif
            <div class="form-group rel focus">
                <label for="">{!! trans("EntityPanel::entitylistdetail.user-selection") !!}</label>
                <select class="nice" name="model_id" required>
                    <option value="">{!! trans("EntityPanel::entitylistdetail.user-selection") !!}</option>
                    @foreach($modulusers as $moduluser)
                        <option @if($user->model_id == $moduluser->id) selected @endif value="{!! $moduluser->id !!}">{!! $moduluser->name !!}</option>
                    @endforeach
                </select>
            </div>
            <div class="float-right">
                <button type="submit" class="btn btn-primary">{!! trans("MPCorePanel::general.save") !!}</button>
            </div>
        </form>
    </div>
@endsection