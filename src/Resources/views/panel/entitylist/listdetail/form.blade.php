@csrf
<div class="form-group rel focus">
    <label for="">{!! trans("EntityPanel::entitylistdetail.user-selection") !!}</label><br/>
    <select id='pre-selected-options' name="users[]" multiple='multiple'>
            <optgroup label="{!! trans("MPCorePanel::general.users") !!}">
                @foreach($users as $user)
                    <option value="{!! $user->id !!}-user">{!! $user->name !!}</option>
                @endforeach
            </optgroup>
            <optgroup label="{!! trans("MPCorePanel::general.entities") !!}">
                @foreach($entities as $entity)
                    <option value="{!! $entity->id !!}-enti">{!! $entity->name !!}</option>
                @endforeach
            </optgroup>
    </select>
</div>
<div class="float-right">
    <button class="btn btn-primary">{!! trans("MPCorePanel::general.save") !!}</button>
</div>
@push('scripts')
    <script type="application/javascript">
        $('#pre-selected-options').multiSelect({
            selectableHeader: "<div class='custom-header'>{!! trans("MPCorePanel::general.eligible") !!}</div>",
            selectionHeader: "<div class='custom-header'>{!! trans("MPCorePanel::general.selected") !!}</div>",
        });
    </script>
@endpush