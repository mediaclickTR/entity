@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <div class="title">{!! trans("EntityPanel::entitylist.edit-entitylist") !!}</div>
        @include("EntityPanel::entitylist.form")
    </div>
@endsection