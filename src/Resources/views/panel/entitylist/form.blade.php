@if(!isset($entitylist))
    {{ Form::open(['route' => 'Entity.entitylist.store']) }}
@else
    {{ Form::model($entitylist, ['route' => ['Entity.entitylist.update'], 'method' => 'POST']) }}
    {!! Form::hidden("id",$entitylist->id) !!}
@endif
@csrf


@push('specific.styles')
    <link rel="stylesheet" href="{!! asset('vendor/mediapress/css/selector.css') !!}" />
@endpush


<div class="form-group rel">
    <span class="tit">{!! trans("EntityPanel::entitylist.select_websites") !!}</span><br/><br/>
    {!!Form::select('website_id[]', $websites,isset($entitylist) ? $entitylist->websites->pluck("id") : session()->get('panel.website')->id, ['class' => 'multiple','multiple', 'required'])!!}
</div>

<div class="form-group rel focus">
    <span class="tit">{!! trans("EntityPanel::entitylist.name-entitylist") !!}</span>
    {!!Form::text('name', null, ["placeholder"=>trans("EntityPanel::entitylist.name-entitylist"), "class"=>"validate[required]"])!!}
</div>

<div class="form-group rel focus">
    <span class="tit">{!! trans("EntityPanel::entitylist.description-entitylist") !!}</span>
    {!!Form::textarea('description', null, ["placeholder"=>trans("EntityPanel::entitylist.description-entitylist")])!!}
</div>

<div class="form-group rel ap">
    <span class="tit">{!! trans("MPCorePanel::general.status") !!}</span>
    <div class="checkbox">
        <label>
            {!! trans("MPCorePanel::general.status_active") !!}
            <input type="radio" @if(isset($entitylist) && $entitylist->status==1) checked @endif name="status" value="1">
        </label>
        <label>
            {!! trans("MPCorePanel::general.status_passive") !!}
            <input type="radio" @if(isset($entitylist) && $entitylist->status==0) checked @endif name="status" value="0">
        </label>
    </div>
</div>

<div class="float-right">
    <button class="btn btn-primary">{!! trans("MPCorePanel::general.save") !!}</button>
</div>

@push("specific.scripts")
    <script src="{!! asset('vendor/mediapress/js/jquery.bootstrap.wizard.min.js') !!}"></script>
    <script src="{!! asset('vendor/mediapress/js/fm.selectator.jquery.js') !!}"></script>
@endpush

@push("scripts")
    <script>
        $(document).ready(function() {
            $('.multiple').selectator({
                showAllOptionsOnFocus: true,
                searchFields: 'value text subtitle right'
            });
        });
    </script>
@endpush