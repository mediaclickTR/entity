@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <div class="title">{!! trans("EntityPanel::entitylist.entitylist") !!}</div>
            @if(count($entitylists)>0)
                <div class="float-left">
                    <select id="multiple-processings" onchange="locationOnChange(this.value);">
                        <option value="">{!! trans('MPCorePanel::general.multiple_transactions') !!}</option>
                        <option  {!! requestHasAndEqual("list","all") ? "selected" : '' !!} value="{!! route("Entity.entitylist.index", array_merge($queries, ["list"=>"all"])) !!}">{!! trans('MPCorePanel::general.all_website_records') !!}</option>
                    </select>
                    @if(!empty($queries))
                        <a href="{!! route("Entity.entitylist.index") !!}" class="filter-block">
                            <i class="fa fa-trash"></i> {!! trans('MPCorePanel::general.clear_filters') !!}
                        </a>
                    @endif
                </div>
            @endif
            <div class="float-right">
                    <a class="btn btn-primary" href="{!! route('Entity.entitylist.create') !!}"> <i class="fa fa-plus"></i> {!! trans("MPCorePanel::general.new_add") !!}</a>
            </div>
            <div class="clearfix"></div>
            <div class="table-field">
                @if(count($entitylists) == 0)
                    <div class="alert alert-warning">{!! trans("MPCorePanel::general.nodata") !!}</div>
                @else
                    <table>
                        <thead>
                        <tr>
                            <th>{!! trans("MPCorePanel::general.status") !!}</th>
                            <th>{!! trans("MPCorePanel::general.id") !!}</th>
                            <th>{!! trans("MPCorePanel::general.name") !!}</th>
                            <th>{!! trans("MPCorePanel::general.website") !!}</th>
                            <th>{!! trans("MPCorePanel::general.type") !!}</th>
                            <th>{!! trans("MPCorePanel::general.users") !!}</th>
                            <th>{!! trans("MPCorePanel::general.created_at") !!}</th>
                            <th>{!! trans("MPCorePanel::general.actions") !!}</th>
                        </tr>
                        </thead>
                        @foreach($entitylists as $entitylist)
                            @php($users = $entitylist->records()->count())
                            <tr>
                                <td>{!! ($entitylist->status == 1) ? '<i class="aktif"></i>' : '<i class="sifreli"></i>' !!}</td>
                                <td>{!! $entitylist->id!!}</td>
                                <td>{!! $entitylist->name !!}</td>
                                <td>
                                    @if($entitylist->websites())
                                        @foreach($entitylist->websites as $site)
                                            {!! $site->domain !!}
                                        @endforeach
                                    @endif
                                </td>
                                <td>{!! ($entitylist->list_type == "standard") ? trans("EntityPanel::entitylist.standard") : trans("EntityPanel::entitylist.core") !!}</td>
                                <td>
                                    @if($users==0)
                                        <a href="{!! route('Entity.entitylist.listdetail.create',$entitylist->id) !!}" class="text-success"><i class="fa fa-plus" style="display:inline"></i> {!! trans("EntityPanel::user.create-user") !!}</a>
                                    @else
                                        <a href="{!! route('Entity.entitylist.listdetail.index',$entitylist->id) !!}" class="text-info"><i class="fa fa-users" style="display:inline;"></i> {!! $users !!}</a>
                                    @endif
                                </td>
                                <td>{!! $entitylist->created_at !!}</td>
                                <td>
                                    <select class="nice" onchange="locationOnChange(this.value);">
                                        <option value="javascript:void(0)">{!! trans("MPCorePanel::general.selection") !!}</option>
                                        <option value="{!! route('Entity.entitylist.edit',$entitylist->id) !!}">{!! trans("MPCorePanel::general.edit") !!}</option>
                                        <option value="{!! route('Entity.entitylist.listdetail.index',$entitylist->id) !!}">{!! trans("EntityPanel::entitylist.list-members") !!}</option>
                                        <option value="{!! route('Entity.entitylist.listdetail.create',$entitylist->id) !!}">{!! trans("EntityPanel::entitylist.list-members-create") !!}</option>
                                        <option value="{!! route('Entity.entitylist.delete',$entitylist->id ) !!}">{!! trans("MPCorePanel::general.delete") !!}</option>
                                    </select>
                                </td>
                            </tr>
                        @endforeach
                    </table>

                    <div class="float-left">
                        {!! $entitylists->links() !!}
                    </div>
                    <div class="float-right">
                        <select onchange="locationOnChange(this.value);">
                            <option value="#">{!! trans('MPCorePanel::general.select.list.size') !!}</option>
                            <option  {!! requestHasAndEqual("list_size",15) ? "selected" : '' !!} value="{!! route("Entity.entitylist.index", array_merge($queries, ["list_size"=>"15"])) !!}">15</option>
                            <option  {!! requestHasAndEqual("list_size",25) ? "selected" : '' !!} value="{!! route("Entity.entitylist.index", array_merge($queries, ["list_size"=>"25"])) !!}">25</option>
                            <option  {!! requestHasAndEqual("list_size",50) ? "selected" : '' !!} value="{!! route("Entity.entitylist.index", array_merge($queries, ["list_size"=>"50"])) !!}">50</option>
                            <option  {!! requestHasAndEqual("list_size",100) ? "selected" : '' !!} value="{!! route("Entity.entitylist.index", array_merge($queries, ["list_size"=>"100"])) !!}">100</option>
                        </select>
                    </div>

                    <div class="edition">
                        <ul>
                            <li>{!! trans("MPCorePanel::general.status_active") !!}</li>
                            <li>{!! trans("MPCorePanel::general.status_passive") !!}</li>
                        </ul>
                    </div>
                @endif
            </div>
        </div>
@endsection

@push("styles")
    <style>
        tr>td {
            text-align:center !important;
        }
    </style>
@endpush
