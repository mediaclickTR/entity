@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <div class="title">{!! trans("EntityPanel::user.create-user") !!}</div>
        @include("EntityPanel::users.form")
    </div>
@endsection