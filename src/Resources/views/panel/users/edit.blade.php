@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <div class="title">{!! trans("EntityPanel::user.edit-user") !!}</div>
        <div class="col-md-12">
            @include("EntityPanel::users.form")
        </div>
    </div>
@endsection