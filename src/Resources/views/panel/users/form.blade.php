@if(!isset($user))
    {{ Form::open(['route' => 'Entity.users.store']) }}
@else
    {{ Form::model($user, ['route' => ['Entity.users.update'], 'method' => 'POST']) }}
    {!! Form::hidden("id",$user->id) !!}
    <input type="hidden" name="old_email" value="{!! $user->email !!}">
@endif
@csrf
<div class="form-group rel">
    <label>{!! trans("EntityPanel::entitylist.select_websites") !!}</label><br/><br/>
    {!!Form::select('website_id', $websites,null, ['class' => 'nice','required', 'placeholder'=>trans("MPCorePanel::general.selection")])!!}
</div>
<div class="form-group rel focus">
    <label>{!! trans("EntityPanel::user.name-user") !!}</label>
    {!!Form::text('name', null, ["placeholder"=>trans("EntityPanel::user.name-user"), "class"=>"validate[required]"])!!}
</div>
<div class="form-group rel focus">
    <label>{!! trans("EntityPanel::user.email-user") !!}</label>
    {!!Form::text('email', null, ["placeholder"=>trans("EntityPanel::user.email-user"), "class"=>"validate[required]"])!!}
</div>
<div class="form-group rel focus">
    <label>{!! trans("EntityPanel::user.password-user") !!}</label>
    <input type="password" id="password" placeholder="{!! trans("EntityPanel::user.password-user") !!}" name="password">
</div>
<div class="form-group rel focus">
    <label>{!! trans("EntityPanel::user.repeat-password-user") !!}</label>
    {!!Form::password('password_confirmation', null, ["placeholder"=>trans("EntityPanel::user.repeat-password-user")])!!}
</div>

<div class="float-right">
    <button type="submit" class="btn btn-primary">{!! trans("MPCorePanel::general.save") !!}</button>
</div>

@push('specific.scripts')
    <script type="text/javascript" src="{!! asset("vendor/mediapress/js/jquery.password-validation.js") !!}"></script>
@endpush

@push("scripts")
    <script>
        //$('#password').passwordStrength();
    </script>
@endpush