<?php

namespace Mediapress\Entity\Http\Controllers;

use Illuminate\Http\Request;
use Mediapress\Entity\Models\EntityList;
use Mediapress\Modules\MPCore\Facades\MPCore;
use Mediapress\Modules\MPCore\Facades\FilterEngine;
use Mediapress\Modules\MPCore\Facades\UserActionLog;
use Mediapress\Http\Controllers\PanelController as Controller;

class EntitylistController extends Controller
{
    public const ACCESSDENIED = 'accessdenied';
    public const ENTITY_PANEL_ENTITYLIST_NAME_USERLIST = "EntityPanel::entitylist.name-userlist";
    public const DESCRIPTION = 'description';
    public const MESSAGE = 'message';
    public const MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE = 'MPCorePanel::general.success_message';

    public function index()
    {
        if (!userAction('message.forms.index', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }
        $entitylists = EntityList::where("id", "<>", 0);
        $queries = FilterEngine::filter($entitylists)['queries'];
        $entitylists = FilterEngine::filter($entitylists)['model'];
        return view('EntityPanel::entitylist.index', compact('entitylists', 'queries'));
    }

    public function create()
    {
        if (!userAction('message.entitylist.create', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $websites = MPCore::getInternalWebsites()->pluck("slug", "id");
        return view("EntityPanel::entitylist.create", compact('websites'));
    }

    public function edit($id)
    {
        $entitylist = EntityList::find($id);
        $websites = MPCore::getInternalWebsites()->pluck("slug", "id");
        return view('EntityPanel::entitylist.edit', compact("entitylist", "websites"));
    }

    public function store(Request $request)
    {
        if (!userAction('message.entitylist.create', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }
        /*
         * Validation
         */
        $fields = [
            'name' => trans(self::ENTITY_PANEL_ENTITYLIST_NAME_USERLIST),
            self::DESCRIPTION => trans("EntityPanel::entitylist.description-userlist"),
        ];

        $rules = [
            'name' => 'required|max:255',
        ];

        $messages = [
            'name.required' => trans("MPCorePanel::validation.filled",
                ['filled', trans(self::ENTITY_PANEL_ENTITYLIST_NAME_USERLIST)]),
        ];

        $this->validate($request, $rules, $messages, $fields);

        $data = request()->except("_token", "website_id");

        $insert = EntityList::firstOrCreate($data);

        if ($insert) {
            foreach ($request->website_id as $website) {
                $insert->websites()->attach([$website]);
            }

            UserActionLog::create(__CLASS__."@".__FUNCTION__, $insert);
        }

        return redirect(route('Entity.entitylist.index'))->with(self::MESSAGE,
            trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function update(Request $request)
    {
        /*
          * Validation
          */

        $fields = [
            'name' => trans(self::ENTITY_PANEL_ENTITYLIST_NAME_USERLIST),
            self::DESCRIPTION => trans("EntityPanel::entitylist.description-userlist"),
        ];

        $rules = [
            'name' => 'required|max:255',
        ];

        $messages = [
            'name.required' => trans("MPCorePanel::validation.filled",
                ['filled', trans(self::ENTITY_PANEL_ENTITYLIST_NAME_USERLIST)]),
        ];

        $this->validate($request, $rules, $messages, $fields);

        $datas = [
            'name' => $request->name,
            self::DESCRIPTION => $request->description,
            'status' => $request->status
        ];


        $update = EntityList::updateOrCreate(['id' => $request->id], $datas);

        if ($update) {

            $update->websites()->detach();
            foreach ($request->website_id as $website) {
                $update->websites()->attach([$website]);
            }

            UserActionLog::update(__CLASS__."@".__FUNCTION__, $update);
        }

        return redirect(route('Entity.entitylist.index'))->with(self::MESSAGE,
            trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function delete($id)
    {
        if (!userAction('message.entitylist.delete', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $data = EntityList::find($id);

        if ($data) {
            $data->delete();
            $data->websites()->detach();
            UserActionLog::delete(__CLASS__."@".__FUNCTION__, $data);
        }

        return redirect()->back()->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }
}
