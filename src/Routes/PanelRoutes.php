<?php


const PREFIX_ENTITY = 'prefix';
const CREATE_ENTITY = '/create';
const STORE_ENTITY = '/store';
const ID_EDIT_ENTITY = '/{id}/edit';
const UPDATE_ENTITY = '/update';
const ID_DELETE_ENTITY = '{id}/delete';
const INDEX_ENTITY = 'index';
const CREATE2_ENTITY = 'create';
const STORE2_ENTITY = 'store';
const EDIT_ENTITY = 'edit';
const UPDATE2_ENTITY = 'update';
const DELETE_ENTITY = 'delete';
Route::group(['middleware' => 'panel.auth'], function()
{
    Route::group([PREFIX_ENTITY => 'Entity', 'as'=>'Entity.'], function () {


        /**
         * Base Modules
         * Include entites, users, userlists
         */

        Route::group([PREFIX_ENTITY => 'Entities', 'as' => 'entities.'], function()
        {
            Route::get('/', ['as' => INDEX_ENTITY, 'uses' => 'EntityController@index']);
            Route::get(CREATE_ENTITY, ['as' => CREATE2_ENTITY, 'uses' => 'EntityController@create']);
            Route::post(STORE_ENTITY, ['as' => STORE2_ENTITY, 'uses' => 'EntityController@store']);
            Route::get(ID_EDIT_ENTITY, ['as' => EDIT_ENTITY, 'uses' => 'EntityController@edit']);
            Route::post(UPDATE_ENTITY, ['as' => UPDATE2_ENTITY, 'uses' => 'EntityController@update']);
        });

        Route::group([PREFIX_ENTITY => 'Users', 'as' => 'users.'], function()
        {
            Route::get('/', ['as' => INDEX_ENTITY, 'uses' => 'UserController@index']);
            Route::get(CREATE_ENTITY, ['as' => CREATE2_ENTITY, 'uses' => 'UserController@create']);
            Route::post(STORE_ENTITY, ['as' => STORE2_ENTITY, 'uses' => 'UserController@store']);
            Route::get(ID_EDIT_ENTITY, ['as' => EDIT_ENTITY, 'uses' => 'UserController@edit']);
            Route::post(UPDATE_ENTITY, ['as' => UPDATE2_ENTITY, 'uses' => 'UserController@update']);
            Route::get(ID_DELETE_ENTITY, ['as' => DELETE_ENTITY, 'uses' => 'UserController@delete']);
        });

        Route::group([PREFIX_ENTITY => 'Entities', 'as' => 'entities.'], function()
        {
            Route::get('/', ['as' => INDEX_ENTITY, 'uses' => 'EntityController@index']);
            Route::get(CREATE_ENTITY, ['as' => CREATE2_ENTITY, 'uses' => 'EntityController@create']);
            Route::post(STORE_ENTITY, ['as' => STORE2_ENTITY, 'uses' => 'EntityController@store']);
            Route::get(ID_EDIT_ENTITY, ['as' => EDIT_ENTITY, 'uses' => 'EntityController@edit']);
            Route::post(UPDATE_ENTITY, ['as' => UPDATE2_ENTITY, 'uses' => 'EntityController@update']);
            Route::get(ID_DELETE_ENTITY, ['as' => DELETE_ENTITY, 'uses' => 'EntityController@delete']);
        });

        Route::group([PREFIX_ENTITY => 'Entitylist', 'as' => 'entitylist.'], function () {
            Route::get('/', ['as' => INDEX_ENTITY, 'uses' => 'EntitylistController@index']);
            Route::get(CREATE_ENTITY, ['as' => CREATE2_ENTITY, 'uses' => 'EntitylistController@create']);
            Route::get('{id}/edit', ['as' => EDIT_ENTITY, 'uses' => 'EntitylistController@edit']);
            Route::post(STORE_ENTITY, ['as' => STORE2_ENTITY, 'uses' => 'EntitylistController@store']);
            Route::post(UPDATE_ENTITY, ['as' => UPDATE2_ENTITY, 'uses' => 'EntitylistController@update']);
            Route::get(ID_DELETE_ENTITY, ['as' => DELETE_ENTITY, 'uses' => 'EntitylistController@delete']);


            Route::group([PREFIX_ENTITY => 'ListDetail/{listid}', 'as' => 'listdetail.'], function () {
                Route::get('/', ['as' => INDEX_ENTITY, 'uses' => 'EntitylistDetailController@index']);
                Route::get(CREATE_ENTITY, ['as' => CREATE2_ENTITY, 'uses' => 'EntitylistDetailController@create']);
                Route::get('/edit/{id}', ['as' => EDIT_ENTITY, 'uses' => 'EntitylistDetailController@edit']);
                Route::get('/detail/{id}', ['as' => 'detail', 'uses' => 'EntitylistDetailController@detail']);
                Route::post(STORE_ENTITY, ['as' => STORE2_ENTITY, 'uses' => 'EntitylistDetailController@store']);
                Route::post(UPDATE_ENTITY, ['as' => UPDATE2_ENTITY, 'uses' => 'EntitylistDetailController@update']);
                Route::get('/delete/{id}', ['as' => DELETE_ENTITY, 'uses' => 'EntitylistDetailController@delete']);
                Route::post('/selectedRemove', ['as' => 'selectedRemove', 'uses' => 'EntitylistDetailController@selectedRemove']);
            });

        });

    });
});
