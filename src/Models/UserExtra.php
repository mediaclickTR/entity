<?php

namespace Mediapress\Entity\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class UserExtra extends Model
{

    use SoftDeletes;

    protected $table = 'user_extras';
    public $timestamps = true;

    protected $fillable = ["user_id", "key", "value"];

    protected $dates = ['deleted_at'];

    public function scopeKey($query, $key)
    {
        return $query->where('key', $key)->first()->value;
    }

}
