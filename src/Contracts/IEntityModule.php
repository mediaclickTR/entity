<?php

namespace Mediapress\Entity\Contracts;

interface IEntityModule extends IModule
{
    public function getEntitylists();

    public function getEntitylist($id);

    public function getEntitylistPivot($pivot_id);

    public function getEntitylistPivotWithEntitylistID($entitylist_id);

    public function getEntitylistDetailWithID($id);

    public function getEntityWithUserableID($model, $userable_id);

    public function getMaillist();
}
