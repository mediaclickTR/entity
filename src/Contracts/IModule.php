<?php

namespace Mediapress\Entity\Contracts;

interface IModule
{
    public function getModuleClasses();

    public function getMPModuleNames();

    public function getPanelMenus();

    public function serveClasses();

    public function getRenderableObjectTypes();

    public function getBreadcrumb($crumbs=[]);
}
