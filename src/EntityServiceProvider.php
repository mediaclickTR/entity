<?php

namespace Mediapress\Entity;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;
use Mediapress\Entity\Facades\Entity;

class EntityServiceProvider extends ServiceProvider
{
    public const CONFIG = 'Config';
    public const ACTIONS_PHP = 'actions.php';
    public const ENTITY = "Entity";

    protected $module_name = self::ENTITY;
    protected $namespace= 'Mediapress\Entity';

    public function boot()
    {
        $this->map();
        $this->loadViewsFrom(__DIR__ . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'panel', $this->module_name.'Panel');
        $this->loadTranslationsFrom(__DIR__ . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . 'panel', $this->module_name.'Panel');
        $this->publishActions(__DIR__);
        $this->loadMigrationsFrom(__DIR__.DIRECTORY_SEPARATOR.'Database'.DIRECTORY_SEPARATOR.'migrations');
        
        $files = $this->app['files']->files(__DIR__ . '/Config');

        foreach ($files as $file) {
            $filename = $this->getConfigBasename($file);
            $this->mergeConfig($file, $filename);
        }

    }

    public function register()
    {
        $loader = AliasLoader::getInstance();
        $loader->alias(self::ENTITY, Entity::class);
        app()->bind(self::ENTITY, function() {
            return new \Mediapress\Entity\Entity;
        });
    }

    protected function publishActions($dir){

        $actions_config = $dir . DIRECTORY_SEPARATOR . self::CONFIG . DIRECTORY_SEPARATOR . self::ACTIONS_PHP;
        if(is_file($actions_config)){
            $this->publishes([
                $dir . DIRECTORY_SEPARATOR . self::CONFIG . DIRECTORY_SEPARATOR . self::ACTIONS_PHP => config_path(strtolower($this->module_name).'_module_actions.php')
            ]);
            $this->mergeConfigFrom($dir . DIRECTORY_SEPARATOR . self::CONFIG . DIRECTORY_SEPARATOR . self::ACTIONS_PHP, strtolower($this->module_name).'_module_actions');
        }

    }

    protected function mergeConfig($path, $key)
    {

        $config = config($key);
        foreach (require $path as $k => $v) {
            if (is_array($v)) {
                if (isset($config[$k])) {
                    $config[$k] = array_merge($config[$k], $v);
                } else {
                    $config[$k] = $v;
                }

            } else {
                $config[$k] = $v;
            }
        }
        config([$key=>$config]);
    }

    protected function getConfigBasename($file)
    {
        return preg_replace('/\\.[^.\\s]{3,4}$/', '', basename($file));
    }

    public function map()
    {
        $this->mapPanelRoutes();
        $this->mapWebRoutes();
    }

    protected function mapWebRoutes()
    {
        $routes_file = __DIR__ .DIRECTORY_SEPARATOR. 'Routes' . DIRECTORY_SEPARATOR . 'WebRoutes.php';

        Route::group([
            'middleware' => ['web', 'mediapress.after'],
            'namespace' => $this->namespace . '\Controllers',
        ], function ($router) use ($routes_file) {
            if(is_file($routes_file)){
                include_once $routes_file;
            }
        });
    }


    protected function mapPanelRoutes()
    {
        $routes_file = __DIR__ .DIRECTORY_SEPARATOR. 'Routes' . DIRECTORY_SEPARATOR . 'PanelRoutes.php';
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace . '\Http\Controllers',
            'prefix' => 'mp-admin',
        ], function ($router) use ($routes_file) {
            if (is_file($routes_file)) {
                include_once $routes_file;
            }
        });
    }
}
