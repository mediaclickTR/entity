<?php

namespace Mediapress\Entity\Facades;

use Illuminate\Support\Facades\Facade;

class Entity extends Facade
{

    protected static function getFacadeAccessor()
    {
        return \Mediapress\Entity\Entity::class;
    }
}
